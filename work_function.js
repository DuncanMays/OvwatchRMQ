/*
 *  @file   work_function.js
 *
 *  @author Duncan Mays, Duncan@distributive.network
 *
 *  modified from the original overwatch work function to operate in node instead of a DCP worker
 **/

// if we store the old require in an object like this it won't be overwritten
const old_require = {'fn': require};

/**
 * A monkey patch for the require function in worker, maps from the DCP module name to the node module name and requires that name instead
 */
function require_node_version(DCP_name){
  console.log('being required: '+DCP_name);
  const module_map = old_require.fn('./DCP_npm_module_map.js');

  node_name = module_map[DCP_name];

  if (node_name == undefined) {
    // if DCP_name isn't in the mapping dict, just use it without alteration
    // this will allow node modules to be loaded through this function, not just DCP modules with node equivalents
    node_name = DCP_name;
  }

  node_module = old_require.fn(node_name);
  return node_module;
}

// sets require to the function defined above which swaps names for DCP modules with node equivalents
// for require statements in this scope
require = require_node_version;
// for require statements in the new Function constructor
global.require = require_node_version;

/**
 * A monkey patch for the progress function in worker, logs progress to the console
 */
function progress(p){
  console.log('Progress is: '+p);
}

/**
 * Work function that is passed to DCP Worker.
 * @param { Array }   sliceData
 * @param { String }  workerInput
 * @param { Object }  labels
 * @returns { Array }
 */
async function workFunction(sliceData, workerInput, labels) {
  progress(0);
  // require('dcp-wasm.js');

  // DECLARE VARIABLES AND FUNCTIONS
  let predTimer   = [];
  let preTimer    = [];
  let postTimer   = [];
  let infTimer    = [];
  let timers      = {};
  let feeds       = {};
  let finalResult = {};
  let preprocess, 
      postprocess, 
      pyodide, 
      pyodideCore;
  let numInputs   = Object.keys(sliceData.b64Data).length;

  function b64ToArrayBuffer(base64) {
    let binary  = atob(base64);
    let len     = binary.length;
    let bytes   = new Uint8Array(len);

    for (let i = 0; i < len; i++) {
      bytes[i] = binary.charCodeAt(i);
    }
    
    return bytes.buffer;
  }

  function createFunction(functionString) {
    return new Function(`return ${ functionString.trim() }`)();
  }
 
  function mapToObj(m){ 
    let obj = Object.fromEntries(m); 
    for (let key of Object.keys(obj)){ 
      if (obj[key].constructor.name == 'Map'){ 
        obj[key] = mapToObj(obj[key]); 
      } 
    } 
    return obj; 
  };

  // DECODE ARGS AND INPUTS
  workerInput     = JSON.parse(workerInput);
  const model     = b64ToArrayBuffer(workerInput.model.model);
  const language  = workerInput.model.language;
  const packages  = workerInput.model.packages;
  const preStr    = workerInput.model.preprocess;
  const postStr   = workerInput.model.postprocess; 

  // CREATE ORT SESSION
  progress(0.1);
  const ort         = require('dcp-ort.js');

  ort.env.wasm.simd = true;
  // const session     = await ort.InferenceSession.create(model, {
  //                       executionProviders: [ 'wasm' ] });
  const session     = await ort.InferenceSession.create(model);
  const inputNames  = session.inputNames;
  const outputNames = session.outputNames;

  progress(0.2);
  let _progress = 0.2;

  if (language == 'javascript') {
    // CREATE JS PRE AND POST PROCESS FUNCTIONS
    preprocess    = createFunction(preStr);
    postprocess   = createFunction(postStr);
  } else if (language == 'python') { 
    // GET PYODIDE CORE
    pyodideCore = require('pyodide-core.js');
    pyodide     = await pyodideCore.pyodideInit();
    
    // PREP PYTHON PACKAGES
    await pyodideCore.loadPackage(packages);

    // PUT STRINGS IN PYTHON SPACE
    globalThis.preStr   = preStr;
    globalThis.postStr  = postStr;

    // WRITE PRE AND POST PROCESS FUNCTIONS TO DISK
    pyodide.runPython(`
import js

with open('./preprocess.py', 'w') as f:
  f.write( js.globalThis.preStr )

with open('./postprocess.py', 'w') as f:
  f.write( js.globalThis.postStr )
    `);

    // IMPORT PRE AND POST PROCESS FUNCTIONS
    preprocess = pyodide.runPython(`
import preprocess 

preprocessFunction = [ i for i in dir(preprocess) if 'preprocess' in i.lower() ]
pythonPre = getattr( preprocess, preprocessFunction[0] )
pythonPre
    `);

    postprocess = pyodide.runPython(`
import postprocess 

postprocessFunction = [ i for i in dir(postprocess) if 'postprocess' in i.lower() ];
pythonPost = getattr( postprocess, postprocessFunction[0] )
pythonPost
    `);
  }

  // CORE LOOP
  for (const [ key, value ] of Object.entries(sliceData.b64Data)) {
    progress(_progress);
    start             = performance.now();

    // DECODE INPUT
    labels['fileID']  = key;
    const b64Input    = value;
    const abInput     = b64ToArrayBuffer(b64Input);

    // RUN JS PREPROCESS
    const preStart    = performance.now();
    if ( language == 'javascript' ) {
      feeds   = preprocess(abInput, ort, inputNames);
    } else if (language == 'python') {
      // CONVERT AB TO BYTES AND RUN PYTHON PREPROCESS
      pyodide.globals.set('preprocessArgs', [abInput, inputNames]);

      pyPreOut = pyodide.runPython(`
import numpy as np
preprocessArgs = preprocessArgs.to_py()
bytesInput = preprocessArgs[0].tobytes()
inputNames = preprocessArgs[1]
inputNames = np.array(inputNames)
feed = pythonPre(bytesInput, inputNames)
feed
      `);

      pyodide.globals.pop('preprocessArgs');

      // CONVERT NP ARRAY TO ONNX TENSOR
      for (const key of pyPreOut.keys()) {
        const value   = pyPreOut.get(key);
        feeds[ key ]  = new ort.Tensor(value.dtype.name, value.getBuffer().data, value.shape.toJs());
      }
    }
    preTimer.push(performance.now() - preStart);

    // RUN INFERENCE
    const infStart    = performance.now();
    let out           = await session.run(feeds);
    infTimer.push(performance.now() - infStart);
    delete feeds;
    const postStart   = performance.now();
    if ( language == 'javascript' ) {
      out   = postprocess(out, labels, outputNames);
    } else if (language == 'python') {
      pyodide.globals.set('postprocessArgs', [out, labels, outputNames]);

      out = pyodide.runPython(`
import numpy as np

postprocessArgs = postprocessArgs.to_py()

outData = postprocessArgs[0]
labels  = postprocessArgs[1]
outputNames = postprocessArgs[2]
for key, value in outData.items():
  outDims   = value.dims.to_py()
  pyOutData = value.data.to_py()
  outType   = str(value.type)
  outData[key] = np.frombuffer(pyOutData.tobytes(), dtype = outType).reshape(outDims)
pyOut = pythonPost(outData, labels, outputNames)
pyOut
      `);
      out = out.toJs();
      out = mapToObj(out);
    }
    postTimer.push(performance.now() - postStart);
    finalResult[key]  = out;
    predTimer.push(performance.now() - start);
    _progress = _progress + (.8 / numInputs);
  }

  const predTime      = predTimer.reduce((initValue, timer) => initValue + timer, 0);
  timers.avgPredTime  = parseFloat((predTime / numInputs).toFixed(3));
  const preTime       = preTimer.reduce((initValue, timer) => initValue + timer, 0);
  timers.avgPreTime   = parseFloat((preTime/numInputs).toFixed(3));
  const infTime       = infTimer.reduce((initValue, timer) => initValue + timer, 0);
  timers.avgInfTime   = parseFloat((infTime/numInputs).toFixed(3));
  const postTime      = postTimer.reduce((initValue, timer) => initValue + timer, 0);
  timers.avgPostTime  = parseFloat((postTime/numInputs).toFixed(3));
  
  // RETURN RESULTS
  outputsAndTimers    = [ finalResult, timers ];
  progress(1);
  return outputsAndTimers;
}

exports.workFunction = workFunction;

