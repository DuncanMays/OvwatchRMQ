
function createFunction(functionString) {
	return new Function(`return ${ functionString.trim() }`)();
}

function fizzbuzz() {
	console.log('fizz');
}

function test_fn(p) {
	require('express');
	fizzbuzz();
	console.log(p);
}

global.require = require
global.fizzbuzz = fizzbuzz

function main() {
	test_fn('hi there!');
	fn = createFunction(test_fn.toString());
	fn('hey!');
}

main();