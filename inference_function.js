
const work_function = require('./work_function.js').workFunction;

const axios = require('axios');

const model_reg_base_url = 'http://192.168.6.227:7000';

/**
 * this function will take an inference task, create the arguements that the overwatch work function expects, and feed them into the work function, returning the result
 */
async function inference_function(inference_task) {

	// deserializing the input
	inference_task = JSON.parse(inference_task.toString());

	images = inference_task.files;
	modelID = inference_task.modelID;
	taskID = inference_task.taskID;

	console.log('Got an inference request for '+images.length+' images on the model '+modelID);

	// we now download the model information from the model registry
	const model_get_url = model_reg_base_url+'/models/'+modelID;
	const model_req = await axios.get(model_get_url);
	const model_info = model_req.data;

	// we now format the images into the input that the work function expects, which is a dict mapping the filename to a binary encoding of the image
	image_dict = {};
	for (let i=0; i<images.length; i++){
		filename = images[i].originalname;
		binary_string = images[i].buffer;
		image_dict[filename] = binary_string;
	}

	// the third parameter, not really sure what it does 
	const labels = {}

	// running the work function
	result = await work_function({'b64Data': image_dict}, JSON.stringify(model_info), labels);

	return result;
}

module.exports = inference_function;