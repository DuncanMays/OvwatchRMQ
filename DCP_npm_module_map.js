
/**
 * This dict maps from DCP module names to the names of corresponding node modules
 */
const module_map = {
	'pyodide-core.js': 'pyodide-core.js',
	'dcp-ort.js': 'onnxruntime-node',
	'decodejpeg/decodeJPEG.js': 'jpeg-js',
	'tf.js': '@tensorflow/tfjs-node',
}

module.exports = module_map