/*
 *  @file  entry_server.js
 *
 *  @author Duncan Mays, Duncan@distributive.network
 *  @date   April 26th, 2023
 *
**/

const amqp = require('amqplib/callback_api');

const inference_function = require('./inference_function.js');

const broker_url = 'amqp://192.168.6.227';
const inference_queue = 'overwatch_inference_queue';

// connecting to the broker
amqp.connect(broker_url, function(error0, connection) {

    // if there's an error establishing a connection, throw it
    if (error0) {
        throw error0;
    }

    // creating a channel to the broker
    connection.createChannel(function(error1, channel) {

        // if an error occurs while creating a channel to the broker
        if (error1) {
            throw error1;
            connection.close()
        }

        // make sure the inference queue exists on the given broker before continuing
        channel.assertQueue(inference_queue, {
            durable: false
        });

        // tells the broker to expect an ack when the task is done, if we don't send and ack, the broker will think this worker is still busy and not send any more tasks. This has the result of deadlocking the worker.
        // another negative result of not sending an ack is that the task will remain on queue, and if there's something about the task that makes workers not ack, the result wil be a Cantor worm
        channel.prefetch(1);

        console.log('Awaiting inference requests');

        // pop off a request off the RPC queue
        channel.consume(inference_queue, async function reply(msg) {
            let return_msg;

            // it's important that any error in worker is caught, or the broker will not recieve any ack, reschedule the task, and the next worker will likely fail too, resulting in a Cantor worm
            try {
                result = await inference_function(msg.content);

                // error code is 0 if there was no error
                return_msg = {
                    'result': result,
                    'error_code': 0
                };

            } catch(error) {
                console.log('there was an error during inference!')
                console.log(error);

                // if there's an error, set error code to 1 and send it back to entry_server through the return queue. Entry_server will recognise the error and rethrow it
                return_msg = {
                    'result': JSON.stringify(error, Object.getOwnPropertyNames(error)),
                    'error_code': 1
                };
            }

            // send the result back through the queue indicated by msg.properties.replyTo
            channel.sendToQueue(msg.properties.replyTo, Buffer.from(JSON.stringify(return_msg)));

            // acknowledges the task is complete so the broker can remove it from the queue, preventing a Cantor worm
            channel.ack(msg);

        });
    });
});

