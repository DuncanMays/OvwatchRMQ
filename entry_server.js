/*
 *  @file  entry_server.js
 *
 *  @author Duncan Mays, Duncan@distributive.network
 *  @date   April 26th, 2023
 *
**/

// lets us interact with the RabbitMQ broker
const amqp = require('amqplib/callback_api');
// an HTTP server library
const express = require('express');
// helps with parsing requests
const multer = require('multer');
// for cross origin requests
cors = require('cors');
// to encode images in binary format
const { encode } = require('base64-arraybuffer');

const broker_url = 'amqp://192.168.6.227';
const inference_queue = 'overwatch_inference_queue';
const port = 8080;
const router = express.Router();
const upload = multer();

/**
 * This function takes a list of inference tasks and pushes them to the RabbitMQ broker for inference on a distributed cluster. This function also creates and subsrices to return queues for workers to push results into, which this function appends into a list and returns to the caller
 */
function rmq_inference(broker_url, inference_tasks) {

	return new Promise((resolve, reject) => {
		
		const num_batches = inference_tasks.length;
		const inference_results = [];

		// create a connection to the broker
		amqp.connect(broker_url, function(error0, connection) {

			// if there's an error while connecting, throw it
			if (error0) {
				console.log('an error occured while connecting to the broker');
				reject(error0);
				return
			}

			// create a channel to the broker
			connection.createChannel(function(error1, channel) {

				// if there's an error while establishing a channel, throw it
				if (error1) {
					console.log('an error occured while creating a channel to the broker');
					// close connection to broker
					connection.close();
					reject(error1);
					return
				}

				// creates a queue with a randomly generated name for inference results, each request will have it's own queue for inference results
				// TODO: check the docs to ensure that autoDelete will set the queue to delete itself once there are no subscribers, otherwise the broker could accumulate empty, unreferenced queues for each inference request it has served in the past
				channel.assertQueue('', {
					exclusive: true,
					autoDelete: true
				}, function(error2, q) {

					// if there's an error while asserting the queue exists, throw it
					if (error2) {
						console.log('an error occured while creating a return queue');
						// close channel and connection to broker
						channel.close()
						connection.close();
						reject(error2);
						return
					}

					// listen to results coming from the return queue that workers push results to
					// q.queue is the randomly generated queue name
					channel.consume(q.queue, function(msg) {

						// append the worker's results to a local list
						inference_results.push(JSON.parse(msg.content.toString()));

						// when the number of recieved results equals the number of batches recieved, close the connection and return the results
						if (inference_results.length >= num_batches) {

							// close channel and connection to broker
							channel.close()
							connection.close();

							// we report the outputs of inferences in an object with input names as keys and inference outputs as values
							let inference_outputs = {};
							// this loop looks for errors and puts all results into the same object
							for (const wrkr_result of inference_results) {

								// if any error occured in worker, reject the promise to the error was returned from this worker
								if (wrkr_result.error_code == 1) {
									reject(wrkr_result.result);
									return
								}

								// if no error occured in this worker, put all the worker's outputs into inference_outputs
								for (const [ filename, result ] of Object.entries(wrkr_result.result[0])) {
									inference_outputs[filename] = result;
								}
							}

							resolve(inference_outputs);
							return
						}
					}, {
						noAck: true
					});

					// sends each inference task to the inference queue
					for (let i=0; i<num_batches; i++) {
						channel.sendToQueue(inference_queue,
							// gives the queue name to the worker for them to push the result into
							Buffer.from(JSON.stringify(inference_tasks[i])), {
								replyTo: q.queue
							}
						);
					}
				}); // the return queue assertion statement
			}); // the channel to broker
		}); // the connection to broker
	}); // the inference_promise
}

/**
 * This route parses incoming requests into batches, which it passes to a separate function that places the batches on queue for distributed inference
 */
async function inference_route(req, res) {

	console.log('recieved a request for inference!');

	res.setHeader('content-type', 'application/json');
	res.header('Access-Control-Allow-Headers', 'content-type');

	const modelID = req.params.publishedName;
	const projectID = req.params.projectID;
	const numInputs = req.files.length;
	const batch_size = parseInt(req.params.batchSize);
	const num_batches = Math.ceil(numInputs / batch_size);

	// we now parse the incomming request into a number of InferenceTasks, or batches
	// these batches contain multiple images and the modelID as well as the taskID
	let inference_tasks = [];

	// serializing each image into base64 format
	let image_b64s = []
	for (index in req.files) {
		req.files[index].buffer = encode(req.files[index].buffer);
	}

	// assembling inference tasks to send to each worker
	for (let i=0; i<num_batches; i++) {

		let inference_task = {
			'files': req.files.slice(i*batch_size, (i+1)*batch_size),
			'modelID': modelID,
			'taskID': projectID
		};

		inference_tasks.push(inference_task);
	}

	try {
		inference_results = await rmq_inference(broker_url, inference_tasks);
		res.send(JSON.stringify(inference_results));

	} catch(error) {
		console.log('error thrown while inferencing with RabbitMQ');
		console.log(error);

		res.status(500);
		res.send(JSON.stringify(error, Object.getOwnPropertyNames(error)));
	}
}

/**
 * @route /overwatch/inference/:projectID/:publishedName/:joinKey/:joinSecret
 * @param {String} projectID
 * @param {String} publishedName
 * @access public
 */
router.post('/Prediction/:projectID/detect/iterations/:publishedName/:batchSize',
	upload.any(),
	inference_route
);

app = express();

app.use('/', router)
app.use(express.urlencoded({ extended: false }));
app.use(express.json({ extended: false }));
app.use(cors());

app.listen(port, () => {
	console.log(`Listening for inference requests on port ${port}`);
});

